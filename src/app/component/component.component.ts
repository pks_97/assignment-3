import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';

@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styleUrls: ['./component.component.css']
})
export class ComponentComponent implements OnInit {

  list:Employee[]
  constructor(){
    this.list=[
      {eid:1,ename:'Rohan',desig:'Analyst',joindate:new Date(2019,10,10),salary:20},
      {eid:2,ename:'Pratyush',desig:'Software Developer',joindate:new Date(2020,17,08),salary:90},
      {eid:3,ename:'Zakir',desig:'Software Developer',joindate:new Date(2010,12,14),salary:45}
    ]
  }
  ngOnInit(): void {
  }

}
